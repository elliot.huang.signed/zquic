const std = @import("std");

fn pkgPath(comptime out: []const u8) std.build.FileSource {
    const outpath = comptime std.fs.path.dirname(@src().file).? ++ std.fs.path.sep_str ++ out;
    return .{ .path = outpath };
}

pub fn build(b: *std.build.Builder) !void {
    const cross_target = b.standardTargetOptions(.{});
    const mode = b.standardReleaseOptions();
    var lsquic_src = std.ArrayList([]const u8).init(b.allocator);
    var cflags = std.ArrayList([]const u8).init(b.allocator);
    const flags = [_][]const u8{
        "-DXXH_HEADER_NAME=\"lsquic_xxhash.h\"",
        "-DLSQPACK_ENC_LOGGER_HEADER=\"lsquic_qpack_enc_logger.h\"",
        "-DLSQPACK_DEC_LOGGER_HEADER=\"lsquic_qpack_dec_logger.h\"",
        "-std=gnu99",
        "-DLSQUIC_CONN_STATS=1",
        "-fno-omit-frame-pointer",
        "-Wno-unused-parameter",
        "-Wno-uninitialized",
        "-Wno-implicit-fallthrough",
    };

    const bssl_flags = [_][]const u8{
        // Assembler option --noexecstack adds .note.GNU-stack to
        // each object to ensure that binaries can be built with
        // non-executable stack.
        "-Wa,--noexecstack",
        "-fvisibility=hidden",
        "-fno-common",
        "-DBORINGSSL_DISPATCH_TEST",
        "-DBORINGSSL_IMPLEMENTATION",
    };

    var bssl_cxxflags = std.ArrayList([]const u8).init(b.allocator);
    var bssl_cflags = std.ArrayList([]const u8).init(b.allocator);
    var crypto_src = std.ArrayList([]const u8).init(b.allocator);
    var ssl_src = std.ArrayList([]const u8).init(b.allocator);
    const fipsmodule = [_][]const u8{
        "deps/boringssl/crypto/fipsmodule/fips_shared_support.c",
        "deps/boringssl/crypto/fipsmodule/bcm.c",
    };

    for (bssl_flags) |flag| {
        try bssl_cflags.append(b.dupe(flag));
        try bssl_cxxflags.append(b.dupe(flag));
    }
    if (cross_target.isLinux()) {
        try bssl_cflags.append("-D_XOPEN_SOURCE=700");
    }

    try bssl_cxxflags.append("-fno-rtti");
    try bssl_cxxflags.append("-fno-exceptions");
    try bssl_cxxflags.append("-std=c++14");
    if (cross_target.isWindows()) {
        try bssl_cxxflags.append("-DWIN32_LEAN_AND_MEAN");
        try bssl_cxxflags.append("-DWIN32");
    }
    // Search for all asm files in `asm_x86_64` and add them
    {
        var dir = try std.fs.cwd().openIterableDir(
            "deps/boringssl/asm_generated",
            .{ .access_sub_paths = true },
        );

        var walker = try dir.walk(b.allocator);
        defer walker.deinit();

        const allowed_exts = [_][]const u8{".S"};
        while (try walker.next()) |entry| {
            const ext = std.fs.path.extension(entry.basename);
            const path = try std.fs.path.join(b.allocator, &[_][]const u8{
                "deps/boringssl/asm_generated",
                entry.path,
            });
            const include_file = for (allowed_exts) |e| {
                if (std.mem.eql(u8, ext, e))
                    break true;
            } else false;
            if (include_file) {
                // we have to clone the path as walker.next()
                // or walker.deinit() will override/kill it
                try crypto_src.append(b.dupe(path));
            }
        }

        for (fipsmodule) |src| {
            try crypto_src.append(b.dupe(src));
        }
    }

    // Search for all c files in `crypto` and add them
    {
        var dir = try std.fs.cwd().openIterableDir(
            "deps/boringssl/crypto",
            .{ .access_sub_paths = true },
        );

        var walker = try dir.walk(b.allocator);
        defer walker.deinit();

        const allowed_exts = [_][]const u8{".c"};
        const disallowed_dirs = [_][]const u8{"fipsmodule"};
        while (try walker.next()) |entry| {
            const ext = std.fs.path.extension(entry.basename);
            const path = try std.fs.path.join(
                b.allocator,
                &[_][]const u8{
                    "deps/boringssl/crypto",
                    entry.path,
                },
            );
            const include_file = for (allowed_exts) |e| {
                if (std.mem.eql(u8, ext, e)) {
                    break true;
                }
            } else false;

            const exclude_dir = for (disallowed_dirs) |d| {
                if (std.mem.containsAtLeast(u8, entry.path, 1, d)) {
                    break true;
                }
            } else false;

            if (include_file and !exclude_dir) {
                // we have to clone the path as walker.next()
                // or walker.deinit() will override/kill it
                try crypto_src.append(b.dupe(path));
            }
        }
        try crypto_src.append("deps/boringssl/err_data/err_data.c");
    }

    const crypto = b.addStaticLibrary("crypto", null);
    crypto.setTarget(cross_target);
    crypto.setBuildMode(mode);
    crypto.linkLibC();
    crypto.addIncludePath("deps/boringssl/include");
    crypto.addCSourceFiles(crypto_src.items, bssl_cflags.items);
    if (cross_target.isWindows() and cross_target.cpu_arch.? == .x86_64) {
        crypto.addIncludePath("deps/clang64/include");
        crypto.addObjectFile("deps/clang64/lib/libpthread.a");
    } else if (cross_target.isWindows() and cross_target.cpu_arch.? == .aarch64) {
        crypto.addIncludePath("deps/clangarm64/include");
        crypto.addObjectFile("deps/clangarm64/lib/libpthread.a");
    }

    // Search for all c files in `crypto` and add them
    {
        var dir = try std.fs.cwd().openIterableDir(
            "deps/boringssl/ssl",
            .{ .access_sub_paths = true },
        );

        var walker = try dir.walk(b.allocator);
        defer walker.deinit();

        const allowed_exts = [_][]const u8{".cc"};
        const disallowed_dirs = [_][]const u8{"test"};
        const disallowed_files = [_][]const u8{"_test.cc"};
        while (try walker.next()) |entry| {
            const ext = std.fs.path.extension(entry.basename);
            const path = try std.fs.path.join(
                b.allocator,
                &[_][]const u8{
                    "deps/boringssl/ssl",
                    entry.path,
                },
            );
            const include_file = for (allowed_exts) |e| {
                if (std.mem.eql(u8, ext, e)) {
                    break true;
                }
            } else false;

            const exclude_dir = for (disallowed_dirs) |d| {
                if (std.mem.containsAtLeast(u8, entry.path, 1, d)) {
                    break true;
                }
            } else false;

            const exclude_file = for (disallowed_files) |f| {
                if (std.mem.containsAtLeast(u8, entry.path, 1, f)) {
                    break true;
                }
            } else false;

            if (include_file and !exclude_dir and !exclude_file) {
                // we have to clone the path as walker.next()
                // or walker.deinit() will override/kill it
                try ssl_src.append(b.dupe(path));
            }
        }
    }

    const ssl = b.addStaticLibrary("ssl", null);
    ssl.setTarget(cross_target);
    ssl.setBuildMode(mode);
    ssl.linkLibCpp();
    ssl.addIncludePath("deps/boringssl/include");
    ssl.addCSourceFiles(ssl_src.items, bssl_cxxflags.items);
    if (cross_target.isWindows() and cross_target.cpu_arch.? == .x86_64) {
        ssl.addIncludePath("deps/clang64/include");
        ssl.addObjectFile("deps/clang64/lib/libpthread.a");
    } else if (cross_target.isWindows() and cross_target.cpu_arch.? == .aarch64) {
        ssl.addIncludePath("deps/clangarm64/include");
        ssl.addObjectFile("deps/clangarm64/lib/libpthread.a");
    }

    var z_src = std.ArrayList([]const u8).init(b.allocator);
    var z_cflags = std.ArrayList([]const u8).init(b.allocator);
    const z_flags = [_][]const u8{
        "-D_LARGEFILE64_SOURCE=1",
    };

    for (z_flags) |flag| {
        try z_cflags.append(b.dupe(flag));
    }

    if (cross_target.isWindows()) {
        try z_cflags.append("-DWIN32_LEAN_AND_MEAN");
        try z_cflags.append("-DWIN32");
        try z_cflags.append("-D_WIN32");
    }

    // Search for all c files in `z` and add them
    {
        var dir = try std.fs.cwd().openIterableDir(
            "deps/zlib",
            .{ .access_sub_paths = true },
        );

        var walker = try dir.walk(b.allocator);
        defer walker.deinit();

        const allowed_exts = [_][]const u8{".c"};
        const disallowed_dirs = [_][]const u8{
            "contrib",
            "examples",
            "test",
        };
        while (try walker.next()) |entry| {
            const ext = std.fs.path.extension(entry.basename);
            const path = try std.fs.path.join(
                b.allocator,
                &[_][]const u8{
                    "deps/zlib",
                    entry.path,
                },
            );

            const include_file = for (allowed_exts) |e| {
                if (std.mem.eql(u8, ext, e)) {
                    break true;
                }
            } else false;

            const exclude_dir = for (disallowed_dirs) |d| {
                if (std.mem.containsAtLeast(u8, entry.path, 1, d)) {
                    break true;
                }
            } else false;

            if (include_file and !exclude_dir) {
                // we have to clone the path as walker.next()
                // or walker.deinit() will override/kill it
                try z_src.append(b.dupe(path));
            }
        }
    }

    const z = b.addStaticLibrary("z", null);
    z.setTarget(cross_target);
    z.setBuildMode(mode);
    z.linkLibC();
    z.addIncludePath("deps/zlib");
    z.addCSourceFiles(z_src.items, z_cflags.items);

    for (flags) |flag| {
        try cflags.append(b.dupe(flag));
    }
    if (cross_target.isWindows()) {
        try cflags.append("-DWIN32_LEAN_AND_MEAN");
        try cflags.append("-DWIN32");
    }

    // Search for all c files in `lsquic` and add them
    {
        var dir = try std.fs.cwd().openIterableDir(
            "deps/lsquic/src/liblsquic",
            .{ .access_sub_paths = true },
        );

        var walker = try dir.walk(b.allocator);
        defer walker.deinit();

        const allowed_exts = [_][]const u8{".c"};
        const disallowed_dirs = [_][]const u8{"ls-qpack"};
        const disallowed_files = [_][]const u8{"common_cert_set"};
        while (try walker.next()) |entry| {
            const ext = std.fs.path.extension(entry.basename);
            const path = try std.fs.path.join(
                b.allocator,
                &[_][]const u8{
                    "deps/lsquic/src/liblsquic",
                    entry.path,
                },
            );
            const exclude_file = for (disallowed_files) |f| {
                if (std.mem.containsAtLeast(u8, entry.basename, 1, f)) {
                    break true;
                }
            } else false;

            const include_file = for (allowed_exts) |e| {
                if (std.mem.eql(u8, ext, e)) {
                    break true;
                }
            } else false;

            const exclude_dir = for (disallowed_dirs) |d| {
                if (std.mem.containsAtLeast(u8, entry.path, 1, d)) {
                    break true;
                }
            } else false;

            if (include_file and !exclude_dir and !exclude_file) {
                // we have to clone the path as walker.next()
                // or walker.deinit() will override/kill it
                try lsquic_src.append(b.dupe(path));
            }
        }
        try lsquic_src.append("deps/lsquic/src/lshpack/lshpack.c");
        try lsquic_src.append("deps/lsquic/src/liblsquic/ls-qpack/lsqpack.c");
    }

    const lsquic = b.addStaticLibrary("lsquic", null);
    lsquic.setTarget(cross_target);
    lsquic.setBuildMode(mode);
    lsquic.linkLibC();
    lsquic.addIncludePath("deps/lsquic/src/liblsquic/ls-qpack");
    lsquic.addIncludePath("deps/lsquic/src/lshpack");
    lsquic.addIncludePath("deps/lsquic/src/liblsquic");
    lsquic.addIncludePath("deps/lsquic/include");
    lsquic.addIncludePath("deps/boringssl/include");
    lsquic.addIncludePath("deps/zlib");
    lsquic.addCSourceFiles(lsquic_src.items, cflags.items);
    lsquic.step.dependOn(&z.step);
    lsquic.step.dependOn(&ssl.step);
    lsquic.step.dependOn(&crypto.step);
    lsquic.linkLibrary(ssl);
    lsquic.linkLibrary(crypto);
    lsquic.linkLibrary(z);
    if (cross_target.isWindows() and cross_target.cpu_arch.? == .x86_64) {
        lsquic.addIncludePath("deps/lsquic/wincompat");
    } else {
        lsquic.addIncludePath("muslcompat");
    }

    const zquic = b.addExecutable("zquic", "src/main.zig");
    zquic.setTarget(cross_target);
    zquic.setBuildMode(mode);
    zquic.linkLibC();
    zquic.linkLibrary(ssl);
    zquic.linkLibrary(crypto);
    zquic.linkLibrary(z);
    zquic.addIncludePath("deps/lsquic/include");
    zquic.addIncludePath("deps/zlib");
    zquic.addIncludePath("deps/boringssl/include");
    zquic.step.dependOn(&lsquic.step);
    zquic.linkLibrary(lsquic);
    zquic.install();
    zquic.addPackage(.{
        .name = "lsquic_c",
        .source = pkgPath("libs/lsquic_c.zig"),
    });
}
