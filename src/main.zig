const std = @import("std");
const os = std.os;
const c = @import("lsquic_c");

const lsquicErr = error{
    InitFail,
};

const client = struct {
    fd: os.fd_t,
    engine: c.lsquic_engine_t,
    method: []u8,
    path: []u8,
    hostname: []u8,
    conn: c.lsquic_conn_t,
    addr: os.sockaddr,
};

pub fn main() !void {
    if (c.lsquic_global_init(c.LSQUIC_GLOBAL_CLIENT) != 0) {
        return lsquicErr.InitFail;
    }

    var settings: c.lsquic_engine_settings = undefined;
    c.lsquic_engine_init_settings(&settings, c.LSENG_HTTP);

    c.lsquic_global_cleanup();
}
